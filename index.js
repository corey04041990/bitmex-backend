const express = require('express')
const bodyParser = require('body-parser')
const app = express()

const getReq = require('./api')

app.use(bodyParser.json())

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

app.use(allowCrossDomain)

// Get instrument active

app.get('/instrument/active', (req, res) => {
    getReq('/api/v1/instrument/active', function (error, response, body) {
        res.json(JSON.parse(body))
    });
})

// Get trade bucketed

app.get('/trade/bucketed', (req, res) => {
    const symbol = req.query.symbol
    getReq(`/api/v1/trade/bucketed?binSize=1m&partial=false&count=100&reverse=true&symbol=${symbol}`,
        function (error, response, body) {
            res.json(JSON.parse(body))
        });
})

// Get order

app.get('/orders', (req, res) => {
    getReq('/api/v1/order', function (error, response, body) {
        res.json(JSON.parse(body))
    });
})

// Post order

app.post('/order', (req, res) => {
    const data = {
        symbol: req.body.symbol,
        orderQty: req.body.orderQty,
        price: req.body.price,
        side: req.body.side,
        ordType: "Limit"
    };

    const postBody = JSON.stringify(data);

    getReq('/api/v1/order', (error, response, body) => {
            res.json(JSON.parse(body))
        },
        'POST', postBody);
})

app.listen(5000)
