const request = require('request');
const crypto = require('crypto');

const apiKey = "w9i0lJU1H8gFbA47X98WX-UL";
const apiSecret = "b8Xnn8uOFaUcfOZl7rNogleqJ0t3Wr1yXtRpgdyWB_K_NWRg";
const REST_API_URL = 'https://testnet.bitmex.com'

function getRequestOptions(verb, path, postBody) {

    const expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future

    const signature = crypto.createHmac('sha256', apiSecret)
        .update(verb + path + expires + postBody)
        .digest('hex');

    const headers = {
        'content-type': 'application/json',
        'Accept': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'api-expires': expires,
        'api-key': apiKey,
        'api-signature': signature
    };

    return {
        headers: headers,
        url: REST_API_URL + path,
        method: verb,
        body: postBody
    }
}

function makeReq(path, callback, method = 'GET', postBody = '') {
    const options = getRequestOptions(method, path, postBody)
    request(options, callback);
}

module.exports = makeReq
